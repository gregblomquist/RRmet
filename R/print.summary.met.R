print.summary.met <-
function(x,...){
cat("\nSample sizes for the groups:\n")
print(x$samples)
cat("\nDescriptive statistics for each variable:\n")
print(x$stats)
}

