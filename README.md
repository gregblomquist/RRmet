RRmet
===========

An `R` package for R-matrix analysis of metric traits, like [Relethford's](https://web.archive.org/web/20180322155803/http://employees.oneonta.edu/relethjh/programs/) `RMET` software. You can install directly from `R` with the following commands. 

```r
install.packages('devtools')
devtools::install_gitlab('gregblomquist/RRmet')
```

Then load the package.

```r
library(RRmet)
```
