\name{irish}
\alias{irish}
\docType{data}
\title{
%%   ~~ data name/kind ... ~~
Relethford's Irish anthropometrics (TEST.MET).
}
\description{
%%  ~~ A concise (1-5 lines) description of the dataset. ~~
Ten anthropometric measurements from 7 locations in western Ireland. This is data distributed with Relethford's Windows RMET program as TEST.MET. It is already converted to an object of class \code{met} and is ready for analysis. Inclusion in the package is primarily for cross-checking results between the R package and Windows RMET.
}
\usage{data(irish)}
\format{
%  The format is:
%List of 5
% $ header: chr "Nineteenth century Irish anthropometrics"
% $ g     : num 7
% $ v     : num 10
% $ info  :'data.frame':	7 obs. of  3 variables:
%  ..$ name  : chr [1:7] "Aran_Islands" "Ballycroy" "Carna" "Garumna" ...
%  ..$ id    : num [1:7] 1 2 3 4 5 6 7
%  ..$ census: num [1:7] 2907 2036 1670 1706 874 ...
% $ dat   :'data.frame':	259 obs. of  11 variables:
%  ..$ group      : num [1:259] 1 1 1 1 1 1 1 1 1 1 ...
%  ..$ HeadLength : num [1:259] 0.653 0.962 1.36 -2.34 -0.325 ...
%  ..$ HeadBreadth: num [1:259] -0.9622 -0.0676 -0.6531 -0.9469 -2.525 ...
%  ..$ Bizygomatic: num [1:259] -0.335 1.206 1.36 -0.778 -1.361 ...
%  ..$ Bigonial   : num [1:259] 0.752 2.89 2.525 -0.116 0.355 ...
%  ..$ NoseLength : num [1:259] 0.1161 2.8897 1.791 -0.0676 1.4655 ...
%  ..$ NoseBreadth: num [1:259] 0.805 0.106 2.212 0.832 0.56 ...
%  ..$ HeadHeight : num [1:259] -1.026 0.917 1.247 -1.411 -0.778 ...
%  ..$ Stature    : num [1:259] -0.418 0.765 2.525 0.116 -0.264 ...
%  ..$ HandLength : num [1:259] -0.6895 0.0386 1.8968 -1.3853 0.1552 ...
%  ..$ Forearm    : num [1:259] -1.8968 0.0773 2.8897 0.3042 -0.7018 ...
% - attr(*, "class")= chr "met"
}
\details{
%%  ~~ If necessary, more details than the __description__ above ~~
}
\source{
%%  ~~ reference to a publication or URL from which the data were obtained ~~
Original data contained in \url{http://konig.la.utk.edu/rmet50.zip}
}
\references{
%%  ~~ possibly secondary sources and usages ~~
Relethford, JH. 1988. Effects of English admixture and geographic distance on anthropometric variation and genetic structure in 19th-century Ireland. \emph{Am J Phys Anthropol} 76:111-124.

Relethford JH, Blangero J. 1990. Detection of differential gene flow from patterns of quantitative variation. \emph{Hum Biol} 62:5-25.

Relethford, JH. 1991. Genetic drift and anthropometric variation in Ireland. \emph{Hum Biol} 63:155-165.
}
\examples{
data(irish)
str(irish)
print(irish)
summary(irish)
}
\keyword{datasets}
